<?php 

// Чтобы показать данные из модели в шаблонах для определённого пользователя, используем метод снизу
use Auth;

view()->composer('frontend.layouts.master', function ($view) {
    $view->with('favourites', \App\Favourite::where('user_id', Auth::user()->id)->get());
});